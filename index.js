const express = require('express')
const app = express()
const port = 3000

const script = `
  fetch(window.location.href, { method: 'HEAD' })
  .then((a) => {
    fetch(window.location.href, { method: 'HEAD', headers: { 'If-None-Match': a.headers.get('Etag') }})
    .then((b) => {
      console.log(b)
      document.getElementById("result").innerHTML = JSON.stringify({
        etag: a.headers.get('Etag'),
        status: b.status
      }, undefined, "  ")
    })
  })
`

app.get('/', (req, res) => {
  res.setHeader('Content-Type', 'text/html')
  res.send(`
    <html>
      <body>
        <h1>ETag issue</h1>
        <div>We want to demonstrate that there is a behaviour difference between Chrome and Firefox with 'ETag' and 'If-None-Match'.</div>
        <br/>
        <div><code id="result" style="white-space: pre"></code></div>
        <br/>
        <div>Status should be equal to 304 for both (see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/If-None-Match">If-None-Match</a>)</div>
        <div>It's the case in Chrome, but in Firefox the status will be 200</div>
        <br/>
        <div>The used fetch script is the following:</div>
        <div><code id="result" style="white-space: pre">${script}</code></div>
      </body>
      <script>${script}</script>
    </html>
  `)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
